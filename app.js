const express = require('express');
const cors = require('cors');
const app = express();
const mongoose = require('mongoose');
//const mongo = require('./libs/mongodb');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Security settings
const helmet = require('helmet');
app.use(helmet.frameguard('deny'));
app.use(helmet.hidePoweredBy());
app.use(helmet.xssFilter());
app.use(helmet.ieNoOpen());

app.get('/status', (req, res) => {
    return res.status(200).end();
});


const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(
    cors({
        origin: '*',
        allowMethods: ['GET','PUT','DELETE'],
        exposeHeaders: ['X-Request-Id']
    }));

// Db connection
connectDb();

// Set up API routes
require('./api/routes/api')(app);


async function connectDb() {
    return await mongoose.connect('mongodb://localhost/wc-timezone');
}

// Error handler
app.use((err, req, res, next) => {
    if (res.headersSent) {
        return next(err);
    }
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    return res.status(err.status);
});

module.exports = app;

