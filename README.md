## Setup - Time Zone

- Prerequisites

    * [NodeJS](https://nodejs.org/en/) with [npm](https://www.npmjs.com/) v6+ or nvm install 8.3.11
    * [MongoDB](https://www.npmjs.com/package/mongodb) - npm install mongodb

## Instructions to run application    

* Clone repository
* Move to project root
* $ run npm install
* $ run npm run start

*Server*

 The server is running locally in http://localhost:5000

*API urls*

* (GET) http://localhost:5000/timezones  
* (GET) http://localhost:5000/timezones/:name
* (PUT) http://localhost:5000/timezones/:name
* (DELETE) http://localhost:5000/timezones/:name  

*Database*

 The db is running on localhost:27017
 - To start a MongoDB process using the init script
    . sudo /etc/init.d/<script-name> start
 - To stop a MongoDB process using its init script, issue:
    . sudo /etc/init.d/<script-name> stop
 - create a db : wc-timezone  (use wc-timezone)


*Features*
- Node
- Express
- MongoDB