const moment = require('moment-timezone');

function getLocalTime(timeZone) {
    console.log('++++++ date:: ', moment().tz(timeZone).format('MMMM Do YYYY, h:mm a z'))
    return moment().tz(timeZone).format('MMMM Do YYYY, h:mm a z');
    
}

module.exports = {
    getLocalTime
}

