
const wcService = require('../services/worldclock_service');

module.exports = {
    getTimeZones,
    getTimeZone,
    updateTimeZone,
    deleteTimeZone
};


async function getTimeZones(req, res, next){
    try {
        const timeZoneList = await wcService.getAllTimeZones()
        return res.status(200).send({ result: timeZoneList });
    } catch (error) {
        return res.status(error.status).send({error_detail: error.message});
    }
} 

async function getTimeZone(req, res, next){
    const { name } = req.params;
    try {
        const timeZone = await wcService.getTimeZone(name)
        return res.status(200).send({time_zone: timeZone});

    } catch (error) {
        console.log(error);
        return res.status(error.status).send({error_detail: error.message});
    }
}

async function updateTimeZone(req, res, next){
    const { name  } = req.params;
    try {
        const timeZone = await wcService.updateTimeZone(name)
        return res.status(200).send({update: 'OK'});
    } catch (error) {
        return next(error);
    }
}

async function deleteTimeZone(req, res, next){
    const { name  } = req.params;
    try {
        const timeZone = await wcService.deleteTimeZone(name)
        return res.status(200).send({delete: 'OK'});
    } catch (error) {
        return next(error);
    }
}