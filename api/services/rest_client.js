const request = require('superagent');

module.exports = {
    _req
};

async function _req(method, uri, data) {
    return request[method](`${uri}`)
    .set('Content-Type', 'application/json')
    .send(data);
}

