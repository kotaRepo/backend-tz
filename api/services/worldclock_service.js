const TimeZoneModel = require('../models/timezone');
const restClient = require('./rest_client')
const { TIME_ZONE_EXTERNAL_URL } = require('../utils/constants')
const { getLocalTime }= require('../utils/dates_format')
const ApplicationError = require('../utils/errors')

module.exports = {
    getAllTimeZones,
    getTimeZone,
    updateTimeZone,
    deleteTimeZone
};

async function getAllTimeZones() {
    console.log('.... test: ');
    try {
        let timeZonesIndb = await getTimeZoneList();
        if(timeZonesIndb.length > 0) {
            return timeZonesIndb;
        }
        
        const timeZoneResult = await restClient._req('get',TIME_ZONE_EXTERNAL_URL);

        const { body:timeZones } = timeZoneResult
        for(i=0; i< timeZones.length; i++) {
            let tz = new TimeZoneModel();
            let sTimeZone = timeZones[i];
            tz.name = sTimeZone.replace('/','-');
            
            tz.name_to_display = timeZones[i];
            await tz.save();
        }
        
        timeZonesIndb = await getTimeZoneList();
        return timeZonesIndb;
    } catch(error) {
        console.log('.... error: ', error);
        throw new ApplicationError(error.message, error.status)
    }
}

async function getTimeZone(timeZoneName) {
    const query = {'name': timeZoneName}
    try {
        const timeZone = await TimeZoneModel.findOne(query);
        if(!timeZone){
            throw new ApplicationError('invalid_time_zone', 400);
        }
        timeZone.local_time =  getLocalTime(timeZone.name_to_display)
        console.log(' ..........found  timezone..', timeZone)
        return timeZone;
        
    } catch (error) {
        console.log('eeee', error)
        throw new ApplicationError(error.message, error.status)
    }
    
}
async function getTimeZoneList(){
    try {
        const timeZoneList = await TimeZoneModel.find();
        return timeZoneList;
    } catch (error) {
        throw error;
    }
}


async function updateTimeZone(timeZoneName) {
   console.log('updating timezone..:', timeZoneName);
   return;
}

async function deleteTimeZone(timeZoneName) {
    console.log('deleting timezone..:', timeZoneName);
    return;
}