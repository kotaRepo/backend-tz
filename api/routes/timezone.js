
const express = require('express');
const router = express.Router();

const worldClock = require('../controllers/wordclock');
module.exports = router;

router.get('/', worldClock.getTimeZones);
router.get('/:name', worldClock.getTimeZone);
router.put('/:name', worldClock.updateTimeZone);
router.delete('/:name', worldClock.deleteTimeZone);


