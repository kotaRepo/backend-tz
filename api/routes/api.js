const express = require('express');
const timeZone = require('./timezone');

function init(app) {

    app.use('/timezones', timeZone);
    
    // catch 404
    app.use((req, res, next) => next(
        res.status(404).send({ code: '404', type: 'route_not_found'})
    ));
}

module.exports = init;

