const mongoose = require('mongoose');
const timestamp = require('mongoose-timestamp');

const schema = new mongoose.Schema({
    name: { type: String },
    name_to_display: { type: String },
    local_time:  {type: String, default: '' }
});

schema.plugin(timestamp);
module.exports = mongoose.model('timezone', schema, 'timezone');