#!/usr/bin/env node

/**
 * Module dependencies.
 */
const app = require('./app');
const http = require('http');
const port = normalizePort('5000'); 

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    const _port = parseInt(val, 10);
    if (isNaN(_port)) {
        return val;
    }
    if (_port >= 0) {
        return _port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
    console.log(error);
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    console.log('Listening on ', bind);
}
